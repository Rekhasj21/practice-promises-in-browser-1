import { createUserTodoElement } from "./display.js"

function getUser(user) {
    return fetch(`https://jsonplaceholder.typicode.com/todos?userId=${user.id}`)
        .then(todoResponse => todoResponse.json())
        .then(todoDetails => {
            const userTodos = {
                name: user.name,
                todos: todoDetails.slice(0, 7)
            }
            return userTodos
        })
        .catch(err => console.error(err.message))
}

fetch('https://jsonplaceholder.typicode.com/users')
    .then(userResponse => userResponse.json())
    .then(userDetails => Promise.all(userDetails.map(user => getUser(user))))
    .then(userTodosList => userTodosList.forEach(userTodos => {
        createUserTodoElement(userTodos);
    }))
    .catch(error => console.error(error));
