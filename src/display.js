const displayContainer = document.getElementById("displayContainer")

export function createUserTodoElement(userTodos) {

    const displayList = document.createElement("div")
    displayList.classList.add('display-list')
    displayContainer.appendChild(displayList)

    const userName = document.createElement('h1')
    userName.classList.add('user-name')
    userName.innerHTML = userTodos.name
    displayList.appendChild(userName)

    const todoListContainer = document.createElement('ul')
    todoListContainer.classList.add('todo-list-container')
    displayList.appendChild(todoListContainer)

    let innerHtmlList = ''
    userTodos.todos.forEach((todo) => {
        const listElement = `<li class="todo-list">
        <input class="checkbox" id='checkbox' type="checkbox"/>
        <label for='checkbox' class="label">${todo.title}</label></li>`
        innerHtmlList += listElement
    });
    todoListContainer.innerHTML = innerHtmlList
}